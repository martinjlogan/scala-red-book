object ExTwo {
  // Ex 2.1
  def fibonacci(n: Int): Int = {
    def loop(n: Int, one: Int, two: Int): Int = n match {
      case 0 | 1 =>  0
      case 2     => two
      case _     => loop(n - 1, two, one + two)
    }
    loop(n, 0, 1)
  }

  private def formatResult(n: Int, f: Int => Int): String = {
    val msg = "Result of %d is %d."
    msg.format(n, f(n))
  }


  // Ex 2.2
  def isSorted[A](as: Array[A], isOrdered: (A,A) => Boolean): Boolean = {
    def loop[B](l: List[A]): Boolean = l match {
      case _ :: Nil                             => true
      case h :: h2 :: tail if(isOrdered(h, h2)) => loop(h2 :: tail)
      case _                                    => false
    }
    loop(as.toList)
  }

  // Ex 2.3
  def curry[A,B,C](f: (A,B) => C): A => (B => C) ={
    (a: A) => (b: B) => f(a, b)
  }

  // Ex 2.4
  def uncurry[A,B,C](f: A => B => C): (A,B) => C = {
    (a: A, b: B) => f(a)(b)
  }

  // Ex 2.5
  def compose[A,B,C](f: B => C, g: A => B): A => C = {
    (a: A) => f(g(a))
  }



  def main(args: Array[String]): Unit = {
    // ex 2.1
    println(formatResult(8, fibonacci))

    // ex 2.2
    println(isSorted(Array(1, 3, 2), (a:Int, b:Int) => a < b))

    // Ex 2.3 & 2.4
    val f = curry((a: Int, b: Int) => (a * b))
    val doubler = f(2)
    println(doubler(4))
    val uncurried = uncurry(f)
    println(uncurried(2, 2))

    // Ex 2.5
    val fg = compose((b: String) => "c", (a: String) => "b")
    println(fg("a") )
  }
}
