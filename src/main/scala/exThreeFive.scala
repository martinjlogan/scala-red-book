sealed trait Tree[+A]
case class Leaf[A](value: A) extends Tree[A]
case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

object exThreeFive {

  def makeTree[A](depth: Int): Tree[Int] = depth match {
    case depth if(depth < 1) => null
    case 1                   => Leaf(util.Random.nextInt(10))
    case _                   => Branch(makeTree(depth - 1), makeTree(depth - 1))
  }

  // Ex 3.26
  def findMax(t: Tree[Int]): Int = t match {
    case Leaf(v)               => v
    case Branch(child, child2) => findMax(child).max(findMax(child2))
  }

  // Ex 3.27
  def findDepth(t: Tree[Int]): Int = t match {
    case Leaf(v)               => 1
    case Branch(child, child2) => (findDepth(child) + 1).max((findDepth(child2) + 1))
  }

  // Ex 3.28
  def map[A](t: Tree[A])(f: A => A): Tree[A] = t match {
    case Leaf(v) => Leaf(f(v))
    case Branch(child, child2) => Branch(map(child)(f), map(child2)(f))
  }

  // Ex 3.29 I would not write this funtion as it drives too much complexity into the lambda and is hideous

  def main(args: Array[String]): Unit ={
    val t = makeTree(3)
    println(t)
    println(findMax(t))
    println(findDepth(t))
    println(map(t)(_ * 2))
  }
}
