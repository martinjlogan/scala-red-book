object Practice {

  def compilesFine[A](as: Array[A], x: A): Boolean = { // x: A fine here
    if(x == 0) true
    else false
  }

  def alsoCompilesFine[A](as: Array[A], x: A): Boolean = {
    def bam(bs: Array[A], y: A): Boolean = { // x: A passed through and used as y: A no problem
      if(y == 0) true
      else false
    }

    bam(as, x)
  }

  def formapper(l: List[Int]): List[Int] = for {
    i <- l
    x <- i.abs 
  } yield {i * x}
  //def noCompile[A](as: Array[A]): Boolean = {
  //  def bam(bs: Array[A], x: A): Boolean = { // x: A in the sub function and no joy
   //   if(x == 0) true
  //    else false
   // }

    //bam(as, Nil)
//  }

  def isSorted[A](as: Seq[A], ordered: (A,A) => Boolean): Boolean = as match {
    case _ :: Nil => true
    case x :: y :: _ if ordered(x,y) => isSorted(as.tail, ordered)
    case _ => false
  }


  def tupleMatch(l: List[Int], l2: List[Int]): Boolean = (l, l2) match {
    case (h :: t, h2 :: t2) => false
    case _ => false
  }

  def main(args: Array[String]): Unit = {
    println(compilesFine(Array(1,2), 1))
    println(compilesFine(Array(1,2), 1))
 //   println(noCompile(Array(1,2)))
    println(isSorted(Seq(1,2,1), (x: Int, y: Int) => x < y))
    println(tupleMatch(List(1,2,3), List(1,2,3)))

<<<<<<< HEAD
    println(formapper(List(1,2,3,4)))
=======
    val v = for {
      i <- List(1,2)
      j <- List(3,4)
    } yield (i,j)

    println(v)

    val i = List(1,2)
    val j = List(3,4)
    val v2 = i.map(e => j.map(e2 => (e, e2)))
    println(v2)
>>>>>>> fcf82d86a7d07b8e22cf40d666f7f5bb80069af8
  }
}
