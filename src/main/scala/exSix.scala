
trait RNG {
  def nextInt: (Int, RNG) // Should generate a random `Int`. We'll later define other functions in terms of `nextInt`.
}


object RNG {
  type Rand[+A] = RNG => (A, RNG)
  type State[S,+A] = S => (A,S)

  case class Simple(seed: Long) extends RNG {
    def nextInt: (Int, RNG) = {
      val newSeed = (seed * 0x5DEECE66DL + 0xBL) & 0xFFFFFFFFFFFFL // `&` is bitwise AND. We use the current seed to generate a new seed.
      val nextRNG = Simple(newSeed) // The next state, which is an `RNG` instance created from the new seed.
      val n = (newSeed >>> 16).toInt // `>>>` is right binary shift with zero fill. The value `n` is our new pseudo-random integer.
      (n, nextRNG) // The return value is a tuple containing both a pseudo-random integer and the next `RNG` state.
    }
  }

  def map[A,B](f: Rand[A])(mf: A => B): Rand[B] = {
    rng => {
      val (n, rng2) = f(rng)
      (mf(n), rng2)
    }
  }

  // ex 6.6
  def map2[A,B,C](fa: Rand[A], fb: Rand[B])(mf: (A, B) => C): Rand[C] = { // These need to be super well defined, they are too tricky to just use casually
    rng => {
      val (v, rng2) = fa(rng)
      val (v2, rng3) = fb(rng)
      (mf(v, v2), rng2)
    }
  }

  def flatmap[A,B](f: Rand[A])(g: A => Rand[B]): Rand[B] = {
    rng =>
      val (a, rng2) = f(rng)
      g(a)(rng2)
  }

    // ex 6.1
  def nonNegInt(rng: RNG): (Int, RNG) = {
    val (i, r) = rng.nextInt
    (i.abs, r)
  }

  def nonNegInt2[B](rng: RNG): (Int, RNG) = {
    map(_.nextInt)((_.abs))(rng)
  }

    // ex 6.3
  def intDouble(rng: RNG): ((Int, Double), RNG) = {
    val (n, rng2) = rng.nextInt
    val (n2, rng3) = rng.nextInt
    ((n, n2.toDouble), rng2)
  }

  def intDouble2(rng: RNG): ((Int, Double), RNG) = {
    map2((_.nextInt), (_.nextInt))((a,b) => (a, a.toDouble))(rng)
  }

  def nonNegativeLessThan(n: Int): Rand[Int] = {
    rng =>
      val (i, rng2) = nonNegInt(rng)
      val mod = i % n
      if (i + (n-1) - mod >= 0)
        (mod, rng2)
      else
        nonNegativeLessThan(n)(rng2)
  }

  def nonNegativeLessThan_(n: Int): Rand[Int] = {
    flatmap(nonNegInt){i =>
        val mod = i % n
        if(i + (n-1) - mod >= 0) (s => (mod, s)) else nonNegativeLessThan_(i)
      }
    }

}


object ExSix {

  def main(args: Array[String]): Unit = {
    val rng = RNG.Simple(42)
    println(rng.nextInt)

    val (_, rng2) =(rng.nextInt)
    println(rng2.nextInt)

    println("non negative int functions")
    println(RNG.nonNegInt(rng2))
    println(RNG.nonNegInt2(rng2))

    println("double functions")
    println(RNG.intDouble(rng2))
    println(RNG.intDouble2(rng2))

    val int: RNG.Rand[Int] = _.nextInt
    println(int(rng))

    println(RNG.map(int)(_ * -1)(rng))

    println(RNG.nonNegativeLessThan(10)(rng))
    println(RNG.nonNegativeLessThan_(10)(rng))

    println("chaining")
    val (_, rngx) = rng.nextInt
    val (_, rngy) = rngx.nextInt
    val (v, _) = rngy.nextInt
    println(v)
    println(
      RNG.flatmap(_.nextInt)(
        {a => RNG.flatmap(_.nextInt)({a => _.nextInt})}
      )(rng)
    )

    // ????? Someone want to explain the get and yield stuff

  }
}
