object ExThree {

  // Ex 3.5
  def dropWhile[A](l: List[A], f: A => Boolean): List[A] = l match {
    case Nil    => Nil
    case h :: t if(f(h)) => dropWhile(t, f)
    case _               => l
  }

  // Ex 3.9 & 3.10
  def foldLeft[A, B](l: List[A], acc: B)(f: (A,B) => B): B = l match {
      case Nil    => acc
      case h :: t => foldLeft(t, f(h, acc))(f)
  }

  def length[A](l: List[A]): Int ={
    foldLeft(l, 0)((_, acc) => acc + 1)
  }

  // Ex 3.24
  def fits[A](sup: List[A], sub: List[A]): Boolean = (sup, sub) match {
    case (h :: t, h2 :: t2) if(h == h2) => fits(t, t2)
    case (_, h2 :: t2)                  => false
    case (_, Nil)                       => true
  }

  def hasSubsequence[A](sup: List[A], sub: List[A]): Boolean = sup match {
    case sup if(sup == sub) => true
    case sup if(sup.length < sub.length) => false
    case _  =>
      fits(sup, sub) match {
        case true  => true
        case false => hasSubsequence(sup.tail, sub)
      }

  }

  def main(args: Array[String]): Unit ={
    // Ex 3.5
    println(dropWhile(List(1,2,3,4,5), (e: Int) => e < 4))

    // Ex 3.9
    println(length(List(1,2,3,4,5)))

    // Ex 3.12
    println(foldLeft(List(1,2,3,4,5), List(0).tail)(_ ::  _))
    //******* HTF do I create an empty list acc that does not cuase some type error println(foldLeft(List(1,2,3,4,5), List(Nil))((e, acc) => e :: acc))

    // Ex 3.16
    println(foldLeft(List(1,2,3,4,5), List(0).tail)(_ + 1 :: _))

    // Ex 3.18
    println(foldLeft(List(1.0,2.0,3.0,4.0,5.0), List(0.0).tail)(_ :: _).toString)

    // Ex 3.24
    println(hasSubsequence(List(2,3), List(2,3,4)))
    println(hasSubsequence(List(1,2,3,4,5), List(2,3,4)))

  }
}
