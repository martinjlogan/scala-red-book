sealed trait Stream[+A] {
  def headOption: Option[A] = this match {
    case Empty => None
    case Cons(a, _) => Some(a())
  }

  def take(n: Int): Stream[A] = {
    def loop(n1: Int, xs: Stream[A]): Stream[A] = xs match {
      case Empty         => Empty
      case _ if(n1 == 0) => Empty
      case Cons(h, t)    => Cons(h, () => loop(n1 - 1, t())) // H is already wrapped in a thunk - why does cons want that and not A
    }
    loop(n, this)
  }

  def toList: List[A] = {
    def loop(xs: Stream[A]): List[A] = xs match {
      case Empty => Nil
      case Cons(h, t) => h() :: loop(t())
    }

    loop(this)
  }

  // ex 5.4
  def forAll(pf: A => Boolean): Boolean = this match {
    case Empty                 => true
    case Cons(h, t) if pf(h()) => t().forAll(pf)
    case _                     => false
  }

}
case object Empty extends Stream[Nothing]
case class Cons[+A](h: () => A, t: () => Stream[A]) extends Stream[A]

object Stream {
  def cons[A](hd: => A, tl: => Stream[A]): Stream[A] = {
    lazy val head = hd
    lazy val tail = tl
    Cons(() => head, () => tail)
  }

  def empty[A]: Stream[A] = Empty

  def apply[A](as: A*): Stream[A] = // How does this work exactly
    if(as.isEmpty) empty else cons(as.head, apply(as.tail: _*))
}

object ExFive {

  val ones: Stream[Int] = Stream.cons(1, ones)

  // ex 5.8
  def constant[A](a: A): Stream[A] = Stream.cons(a, constant(a))

  // ex 5.9
  def from(a: Int): Stream[Int] = Stream.cons(a, from(a + 1)) // If I make this type A I get that 1 should be a string, why

  def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A] =
    f(z) match {
      case Some((h, s)) => Stream.cons(h, unfold(s)(f))
      case None         => Stream.empty
    }


  def maybeTwice(b: Boolean, i: => Int) =
    if (b) i+i
    else 0

  def main(args: Array[String]): Unit = {
    maybeTwice(true, { println("hi"); 5})

    val xs = Stream(1,2,3)
    println(xs.headOption)
    println(xs.toList)
    println(xs.take(2).toList)
    println(xs.forAll((a:Int) => if(a == a) true else false))
    println(ones.take(4).toList)
    println(constant("q").take(4).toList)
    println(from(3).take(4).toList)
    println(unfold(1)(s => Some(s, s + 1)).take(4).toList)

  }
}
