sealed trait Option[+A] {
  def map[B](f: A => B): Option[B] = this match {
    case None    => None
    case Some(a) => Some(f(a))
  }
  def flatmap[B](f: A => Option[B]): Option[B] = this match {
    case None => None
    case Some(a) => f(a)
  }
  def getOrElse[B >: A](default: => B): B = this match {
    case None => default
    case Some(a) => a
  }
  def orElse[B >: A](ob: => Option[B]): Option[B] = this match {
    case None => ob
    case b => b
  }
  def filterP(f: A => Boolean): Option[A] = this match {
    case None => None
    case Some(a) => if(f(a)) this else None
  }
  def filter(f: A => Boolean): Option[A] =
    this.flatmap(
      (a) => if(f(a)) Some(a) else None
    )


}
case class Some[+A](get: A) extends Option[A]
case object None extends Option[Nothing]


object ExFour {
  def mean(xs: Seq[Double]): Option[Double] = xs match {
    case Nil => None
    case _   => Some(xs.sum / xs.length)
  }

  // Ex 4.2
  def variance(xs: Seq[Double]): Option[Double] =
    mean(xs).flatmap((m) =>
                  mean(xs.map((e) => math.pow(e - m, 2)))
                )

  def lift[A,B](f: A => B): Option[A] => Option[B] =
    (_ map f)


  def main(args: Array[String]): Unit = {
    println(mean(Seq(1,2)))
    assert(None == mean(Seq()))

    println(mean(Seq(1,2)).map(_ * 2))
    println(mean(Seq()).map(_ * 2))

    // This now seems to return either a double or a string?
    println(mean(Seq(1,2)).getOrElse("error"))
    println(mean(Seq()).getOrElse("error"))

    println(mean(Seq(1,2)).orElse(Some("error")))
    println(mean(Seq()).orElse(Some("error")))

    println(mean(Seq(1,2)).filter(_ == 3.0))
    println(mean(Seq()).filter(_ == 3.0))

    // Ex 4.2
    println(variance(Seq(1,2)))

    // lift
    val f = (a: Int) => a * 2
    println(lift(f)(Some(2)))

  }
}
